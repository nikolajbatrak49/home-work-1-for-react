import { Component } from "react";
import { Button } from "../button";

import styles from "../../styles/modal.module.scss";

class Modal extends Component {
    render() {
      const { visible, header, closeButton, close, text, actions} = this.props;
      return visible && (
        <div
          className={styles.modalWrapper}
          onClick={(e) => e.currentTarget === e.target && this.props.close()}
        >
          <div className={styles.modal}>
            <div className={styles.modalHeader}>
              {header ? <h2>{header}</h2> : null}
              {closeButton && (
                <Button
                  text="X"
                  backgroundColor="rgb(163, 35, 0)"
                  onClick={close}
                />
              )}
            </div>
            <div className={styles.modalContent}>{text}</div>
            <div className={styles.modalFooter}>{actions}</div>
          </div>
        </div>
      );
    }
}
  
  
  export default Modal;
