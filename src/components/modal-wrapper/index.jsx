
import { Component } from "react";
// import styles from "../../styles/app.module.scss";
import  Modal from "../modal/Modal";
import { Button } from "../button";

export default class ModalWrapper extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
    };
  }

  openModal = () => {
    this.setState({ modalVisible: true });
  };

  closeModal = () => {
    this.setState({ modalVisible: false });
  };

  render() {
    const modalVisible = this.state.modalVisible;
    const { header, textModal, closeButton, textButton, actions} = this.props;
    return (
      <>
        <Button
          text={textButton}
          onClick={this.openModal}
        />
        <Modal
          visible={modalVisible}
          header={header}
          text={textModal}
          closeButton={closeButton}
          close={this.closeModal}
          actions={actions}
        />
      </>
    );
  }
}
