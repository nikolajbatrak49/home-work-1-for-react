import { Component } from "react";
import styles from "../../styles/Button.module.scss";

class Button extends Component {
  render() {
    const { text, backgroundColor, onClick } = this.props;
    return (
      <button className={styles.button} onClick={onClick} style={{backgroundColor: backgroundColor}}>
        {text}
      </button>
    );
  }
}

export default Button;
