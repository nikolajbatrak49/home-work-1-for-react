
import { Component } from "react";

import ModalWrapper from "./components/modal-wrapper";
import Button from "./components/button/Button";

import styles from "./styles/app.module.scss";


class App extends Component {

  send = () => {
    console.log('you send message');
  };

  render() {
    return (
      <div className={styles.App}>
        <ModalWrapper
          header = "Do you want to delete this file?"
          textModal = "Once you delete this file, it won`t be possible to undo this action. Are you sure you want to delete it?"
          closeButton = {true}
          textButton = "Open first modal"
          actions={[
            <Button
              text="Ok"
              onClick={this.send}
              key={Date.now()}
              backgroundColor="rgba(116, 26, 32, 0.708)"
            />,
            <Button
              text="Cancel"
              onClick={this.closeModal}
              key={Date.now() + 1}
              backgroundColor="rgba(116, 26, 32, 0.708)"
            />,
          ]}
        />
        <ModalWrapper
          header = "Second modal title"
          textModal = "second modal text"
          closeButton = {true}
          textButton = "Open second modal"
          actions={[
            <Button
              text="OK"
              onClick={this.send}
              key={Date.now()}
              backgroundColor="red"
            />,
            <Button
              text="Cancel"
              onClick={this.closeModal}
              key={Date.now() + 1}
              backgroundColor="red"
            />,
          ]}
        />
      </div>
    );
  }
}


export default App;
